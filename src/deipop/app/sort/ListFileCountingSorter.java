package deipop.app.sort;

import deipop.app.file.ArrayFileModifier;
import deipop.app.generate.FileGenerator;

import java.io.File;

public class ListFileCountingSorter extends ListFileSorter {

    public ListFileCountingSorter(File file) {
        super(file);
    }

    @Override
    public void sort() {
        countingSort();

        fileModifier.close();
    }

    private void countingSort() {
        int lowestValue = getLowestValue();
        int highestValue = getHighestValue();

        fileModifier.seekHead();

        int range = highestValue - lowestValue + 1;

        ArrayFileModifier tempFileModifier = new ArrayFileModifier(new File("./temp_counting_sort_array.txt"));

        for (int i = 0; i < range; i++) {
            tempFileModifier.writeInt(0, true);
        }

        for (int i = 0; i < FileGenerator.LINE_COUNT; i++) {
            int number = fileModifier.readInt(i);

            tempFileModifier.writeInt(tempFileModifier.readInt(number - lowestValue) + 1, number - lowestValue);
        }

        int z = 0;

        for (int i = lowestValue; i <= highestValue; i++) {
            while (tempFileModifier.readInt(i - lowestValue) > 0) {
                fileModifier.writeInt(i, z);
                z++;
                tempFileModifier.writeInt(tempFileModifier.readInt(i - lowestValue) - 1, i - lowestValue);
            }
        }

        tempFileModifier.close();
        tempFileModifier.deleteFile();
    }

    private int getLowestValue() {
        fileModifier.seekHead();

        int lowestValue = fileModifier.readInt(0);

        for (int i = 0; i < FileGenerator.LINE_COUNT; i++) {
            int value = fileModifier.readInt(i);

            if (value < lowestValue) {
                lowestValue = value;
            }
        }

        return lowestValue;
    }

    private int getHighestValue() {
        fileModifier.seekHead();

        int highestValue = fileModifier.readInt(0);

        for (int i = 0; i < FileGenerator.LINE_COUNT; i++) {
            int value = fileModifier.readInt(i);

            if (value > highestValue) {
                highestValue = value;
            }
        }

        return highestValue;
    }

}
