package deipop.app.sort;

import deipop.app.file.ListFileModifier;

import java.io.File;

public abstract class ListFileSorter extends FileSorter<ListFileModifier> {

    public ListFileSorter(File file) {
        super(new ListFileModifier(file));
    }

}