package deipop.app.sort;

import deipop.app.generate.FileGenerator;

import java.io.File;

public class ListFileQuickSorter extends ListFileSorter {

    public ListFileQuickSorter(File file) {
        super(file);
    }

    @Override
    public void sort() {
        fileModifier.seekHead();

        quickSort(0, FileGenerator.LINE_COUNT - 1);

        fileModifier.close();
    }

    private void quickSort(int low, int high) {
        int i = low;
        int j = high;

        int pivot = fileModifier.readInt(low + (high - low) / 2);

        while (i <= j) {
            while (fileModifier.readInt(i) < pivot) {
                i++;
            }

            while (fileModifier.readInt(j) > pivot) {
                j--;
            }

            if (i <= j) {
                switchNumbers(i, j);

                i++;
                j--;
            }
        }

        if (low < j) {
            quickSort(low, j);
        }

        if (i < high) {
            quickSort(i, high);
        }
    }

    private void switchNumbers(int i, int j) {
        int temp = fileModifier.readInt(i);
        fileModifier.writeInt(fileModifier.readInt(j), i);
        fileModifier.writeInt(temp, j);
    }

}
