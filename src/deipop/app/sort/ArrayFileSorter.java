package deipop.app.sort;

import deipop.app.file.ArrayFileModifier;

import java.io.File;

public abstract class ArrayFileSorter extends FileSorter<ArrayFileModifier> {

    public ArrayFileSorter(File file) {
        super(new ArrayFileModifier(file));
    }

}