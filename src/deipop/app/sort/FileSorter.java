package deipop.app.sort;

import deipop.app.file.FileModifier;

public abstract class FileSorter<E extends FileModifier> {

    protected final E fileModifier;

    public FileSorter(E fileModifier) {
        this.fileModifier = fileModifier;
    }

    public abstract void sort();

}