package deipop.app.sort;

import deipop.app.file.ArrayFileModifier;
import deipop.app.generate.FileGenerator;

import java.io.File;

public class ArrayFileMergeSorter extends ArrayFileSorter {

    public ArrayFileMergeSorter(File file) {
        super(file);
    }

    @Override
    public void sort() {
        mergeSort(0, FileGenerator.LINE_COUNT - 1);

        fileModifier.close();
    }

    private void mergeSort(int low, int high) {
        if (low < high) {
            int middle = low + (high - low) / 2;

            mergeSort(low, middle);
            mergeSort(middle + 1, high);
            mergeParts(low, middle, high);
        }
    }

    private void mergeParts(int low, int middle, int high) {
        ArrayFileModifier tempFileModifier = new ArrayFileModifier(fileModifier.getFile(), true);

        int i = low;
        int j = middle + 1;
        int k = low;

        while (i <= middle && j <= high) {
            int value1 = tempFileModifier.readInt(i);
            int value2 = tempFileModifier.readInt(j);

            if (value1 <= value2) {
                fileModifier.writeInt(value1, k++);
                i++;
            } else {
                fileModifier.writeInt(value2, k++);
                j++;
            }
        }

        while (i <= middle) {
            fileModifier.writeInt(tempFileModifier.readInt(i++), k++);
        }

        tempFileModifier.close();
        tempFileModifier.deleteFile();
    }

}
