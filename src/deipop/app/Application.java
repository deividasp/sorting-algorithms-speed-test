package deipop.app;

import deipop.app.generate.ArrayFileGenerator;
import deipop.app.generate.FileGenerator;
import deipop.app.generate.ListFileGenerator;
import deipop.app.sort.*;
import deipop.app.util.FileUtils;

import java.io.File;

public class Application {

    public static void main(String[] arguments) {
        File arrayFile1 = new File("./merge_sorted_array.txt");
        File listFile1 = new File("./merge_sorted_list.txt");

        new ArrayFileGenerator(arrayFile1).generate();
        new ListFileGenerator(listFile1).generate();

        File arrayFile2 = FileUtils.copy(arrayFile1, new File("./quick_sorted_array.txt"));
        File listFile2 = FileUtils.copy(listFile1, new File("./quick_sorted_list.txt"));

        File arrayFile3 = FileUtils.copy(arrayFile1, new File("./counting_sorted_array.txt"));
        File listFile3 = FileUtils.copy(listFile1, new File("./counting_sorted_list.txt"));

        long startTime;

        ArrayFileMergeSorter afms = new ArrayFileMergeSorter(arrayFile1);
        startTime = System.currentTimeMillis();
        afms.sort();
        System.out.println("Array file merge sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");

        ListFileMergeSorter lfms = new ListFileMergeSorter(listFile1);
        startTime = System.currentTimeMillis();
        lfms.sort();
        System.out.println("List file merge sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");

        ArrayFileQuickSorter afqs = new ArrayFileQuickSorter(arrayFile2);
        startTime = System.currentTimeMillis();
        afqs.sort();
        System.out.println("Array file quick sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");

        ListFileQuickSorter lfqs = new ListFileQuickSorter(listFile2);
        startTime = System.currentTimeMillis();
        lfqs.sort();
        System.out.println("List file quick sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");

        ArrayFileCountingSorter afcs = new ArrayFileCountingSorter(arrayFile3);
        startTime = System.currentTimeMillis();
        afcs.sort();
        System.out.println("Array file counting sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");

        ListFileCountingSorter lfcs = new ListFileCountingSorter(listFile3);
        startTime = System.currentTimeMillis();
        lfcs.sort();
        System.out.println("List file counting sorting for " + FileGenerator.LINE_COUNT + " elements took: " + (System.currentTimeMillis() - startTime) + " ms.");
    }

}