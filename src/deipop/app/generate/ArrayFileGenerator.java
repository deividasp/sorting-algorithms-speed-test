package deipop.app.generate;

import deipop.app.util.RandomUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ArrayFileGenerator extends FileGenerator {

    public static final int CHARS_FOR_VALUE = 11;

    public ArrayFileGenerator(File file) {
        super(file);
    }

    @Override
    public void generate() {
        try {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
                for (int line = 0; line < FileGenerator.LINE_COUNT; line++) {
                    String value = String.valueOf(RandomUtils.getRandomInt(Short.MIN_VALUE, Short.MAX_VALUE));

                    while (value.length() != CHARS_FOR_VALUE) {
                        value += " ";
                    }

                    writer.write(value);
                    writer.newLine();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

}