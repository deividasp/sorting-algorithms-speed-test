package deipop.app.generate;

import deipop.app.util.RandomUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ListFileGenerator extends FileGenerator {

    public static final int CHARS_FOR_VALUE = 11;
    public static final int CHARS_FOR_SEPARATOR = 1;
    public static final int CHARS_FOR_INDEX = 11;

    public static final char INDEX_SEPARATOR = ',';

    public ListFileGenerator(File file) {
        super(file);
    }

    @Override
    public void generate() {
        try {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
                for (int line = 0; line < FileGenerator.LINE_COUNT; line++) {
                    String value = String.valueOf(RandomUtils.getRandomInt(Short.MIN_VALUE, Short.MAX_VALUE));

                    while (value.length() != CHARS_FOR_VALUE) {
                        value += " ";
                    }

                    String prevIndex = String.valueOf(line - 1);

                    if (line - 1 < 0) {
                        prevIndex = "-1";
                    }

                    while (prevIndex.length() != CHARS_FOR_INDEX) {
                        prevIndex += " ";
                    }

                    String nextIndex = String.valueOf(line + 1);

                    if (line + 1 == FileGenerator.LINE_COUNT) {
                        nextIndex = "-1";
                    }

                    while (nextIndex.length() != CHARS_FOR_INDEX) {
                        nextIndex += " ";
                    }

                    writer.write(value + INDEX_SEPARATOR + prevIndex + INDEX_SEPARATOR + nextIndex);
                    writer.newLine();
                }
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

}