package deipop.app.generate;

import java.io.File;

public abstract class FileGenerator {

    public static final int LINE_COUNT = 250;
    public static final int BYTES_FOR_LINE_TERMINATION = 2;

    protected final File file;

    public FileGenerator(File file) {
        this.file = file;
    }

    public abstract void generate();

}