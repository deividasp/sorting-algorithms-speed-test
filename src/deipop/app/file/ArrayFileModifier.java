package deipop.app.file;

import deipop.app.generate.ArrayFileGenerator;
import deipop.app.generate.FileGenerator;

import java.io.File;
import java.io.IOException;

public class ArrayFileModifier extends FileModifier {

    public ArrayFileModifier(File file) {
        super(file);
    }

    public ArrayFileModifier(File file, boolean copy) {
        super(file, copy);
    }

    public void writeInt(int value, boolean newLine) {
        String valueString = String.valueOf(value);

        while (valueString.length() < ArrayFileGenerator.CHARS_FOR_VALUE) {
            valueString += " ";
        }

        if (newLine) {
            valueString += "\r\n";
        }

        try {
            randomAccessFile.write(valueString.getBytes());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void writeInt(int value, int index) {
        seek(index);

        writeInt(value, false);
    }

    public int readInt(int index) {
        seek(index);

        try {
            return Integer.parseInt(randomAccessFile.readLine().replaceAll(" ", ""));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return -1;
    }

    @Override
    protected int getBytesPerLine() {
        return ArrayFileGenerator.CHARS_FOR_VALUE + FileGenerator.BYTES_FOR_LINE_TERMINATION;
    }

}