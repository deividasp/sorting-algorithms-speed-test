package deipop.app.file;

import deipop.app.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class FileModifier {

    private final File file;

    protected RandomAccessFile randomAccessFile;

    public FileModifier(File file) {
        this(file, false);
    }

    public FileModifier(File file, boolean copy) {
        this.file = copy ? FileUtils.copy(file) : file;

        try {
            randomAccessFile = new RandomAccessFile(this.file, copy ? "r" : "rw");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void seek(int line) {
        try {
            randomAccessFile.seek(line * getBytesPerLine());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void close() {
        try {
            randomAccessFile.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void deleteFile() {
        file.delete();
    }

    public File getFile() {
        return file;
    }

    protected abstract int getBytesPerLine();

}