package deipop.app.file;

import deipop.app.generate.FileGenerator;
import deipop.app.generate.ListFileGenerator;

import java.io.File;
import java.io.IOException;

public class ListFileModifier extends FileModifier {

    public ListFileModifier(File file) {
        super(file);
    }

    public ListFileModifier(File file, boolean copy) {
        super(file, copy);
    }

    public void seekHead() {
        for (int i = 0; i < FileGenerator.LINE_COUNT; i++) {
            seek(i);

            if (readNextIndex() == 1) {
                break;
            }
        }
    }

    public void next() {
        if (readNextIndex() != -1) {
            seek(readNextIndex());
        }
    }

    public void prev() {
        if (readPrevIndex() != -1) {
            seek(readPrevIndex());
        }
    }

    public void writeInt(int value, int index) {
        if (index < readCurrentIndex()) {
            while (readCurrentIndex() != index) {
                prev();
            }
        } else if (index > readCurrentIndex()) {
            while (readCurrentIndex() != index) {
                next();
            }
        }

        String valueString = String.valueOf(value);

        while (valueString.length() < ListFileGenerator.CHARS_FOR_VALUE) {
            valueString += " ";
        }

        String prevIndexString = String.valueOf(readPrevIndex());

        while (prevIndexString.length() < ListFileGenerator.CHARS_FOR_INDEX) {
            prevIndexString += " ";
        }

        String nextIndexString = String.valueOf(readNextIndex());

        while (nextIndexString.length() < ListFileGenerator.CHARS_FOR_INDEX) {
            nextIndexString += " ";
        }

        try {
            randomAccessFile.write((valueString + ListFileGenerator.INDEX_SEPARATOR + prevIndexString + ListFileGenerator.INDEX_SEPARATOR + nextIndexString + "\r\n").getBytes());

            if (index == FileGenerator.LINE_COUNT - 1) {
                randomAccessFile.seek(randomAccessFile.getFilePointer() - getBytesPerLine());
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public int readInt(int index) {
        if (index < readCurrentIndex()) {
            while (readCurrentIndex() != index) {
                prev();
            }
        } else if (index > readCurrentIndex()) {
            while (readCurrentIndex() != index) {
                next();
            }
        }

        try {
            int value = Integer.parseInt(randomAccessFile.readLine().split(",")[0].replaceAll(" ", ""));
            randomAccessFile.seek(randomAccessFile.getFilePointer() - getBytesPerLine());
            return value;
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return -1;
    }

    public int readCurrentIndex() {
        return readPrevIndex() + 1;
    }

    public int readPrevIndex() {
        try {
            int index = Integer.parseInt(randomAccessFile.readLine().split(",")[1].replaceAll(" ", ""));
            randomAccessFile.seek(randomAccessFile.getFilePointer() - getBytesPerLine());
            return index;
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return -1;
    }

    public int readNextIndex() {
        try {
            int index = Integer.parseInt(randomAccessFile.readLine().split(",")[2].replaceAll(" ", ""));
            randomAccessFile.seek(randomAccessFile.getFilePointer() - getBytesPerLine());
            return index;
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return -1;
    }

    @Override
    protected int getBytesPerLine() {
        return ListFileGenerator.CHARS_FOR_VALUE + ListFileGenerator.CHARS_FOR_SEPARATOR * 2 + ListFileGenerator.CHARS_FOR_INDEX * 2 + FileGenerator.BYTES_FOR_LINE_TERMINATION;
    }

}