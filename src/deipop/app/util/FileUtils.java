package deipop.app.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileUtils {

    public static final String FILE_COPY_SUFFIX = "_copy";

    public static File copy(File source, File target) {
        try {
            target.createNewFile();
            return Files.copy(Paths.get(source.getPath()), Paths.get(target.getPath()), StandardCopyOption.REPLACE_EXISTING).toFile();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public static File copy(File file) {
        try {
            return Files.copy(Paths.get(file.getPath()), Paths.get(getPathWithoutExtension(file) + FILE_COPY_SUFFIX + getExtension(file)), StandardCopyOption.REPLACE_EXISTING).toFile();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private static String getPathWithoutExtension(File file) {
        return file.getPath().substring(0, file.getPath().lastIndexOf('.'));
    }

    private static String getExtension(File file) {
        return file.getPath().substring(file.getPath().lastIndexOf('.'), file.getPath().length());
    }

}