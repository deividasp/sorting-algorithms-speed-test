package deipop.app.util;

import java.util.Random;

public class RandomUtils {

    public static int getRandomInt(int from, int to) {
        return new Random().nextInt(to - from) + from;
    }

}